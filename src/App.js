import './App.css';

export const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Demo App</h1>
        <h2>Built on Tuesday Feb. 14th 2023 - Valentine's Day ❤️</h2>
      </header>
    </div>
  );
}