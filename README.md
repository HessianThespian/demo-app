# Demo App

## Requirements

* [Node](https://nodejs.org/en/download/) - For local development of React app
* [Docker Desktop](https://docs.docker.com/desktop/install/windows-install/) and [Docker Hub](https://hub.docker.com/) account, with at least one [repository](https://docs.docker.com/docker-hub/repos/) - Name of repository must match name of Docker image further down (e.g. ``demo-app``)
* [Gitlab](https://gitlab.com/) account - For CI/CD
* [Azure account](https://azure.microsoft.com/en-us/free/) and [subscription](https://learn.microsoft.com/en-us/dynamics-nav/how-to--sign-up-for-a-microsoft-azure-subscription) - For hosting application on [App Service](https://azure.microsoft.com/en-us/products/app-service/#overview)

## Setup and testing

### Verify demo application

* Fork this project, then clone it to your local machine
* From the root folder, open a terminal and run:
  * ``npm install``
  * ``npm start``
  * Check http://localhost:3000
* Stop the application

### Verify Docker

Ensure Docker is installed, running, and that you are authenticated. ``<your username>`` refers to your Docker Hub username.

* Build image: From the root folder, open a terminal and run:
  * ``docker build -t <your username>/demo-app .``

Open Docker Desktop and verify that the image exists under "Images" as you named it (``<your username>/demo-app``).

* Run the application as a container: Open a terminal (from anywhere) and run:
  * ``docker run --name demo-app -dp 3000:3000 <your username>/demo-app``

Open Docker Desktop and verify under "Containers" that the container is running (``<your username>/demo-app``). Verify by clicking on the "Port(s)" value (will open browser to display applicaton).

* Push image to Docker Hub; open a terminal (from anywhere) and run:
  * ``docker push <your username>/demo-app[:tagname]``

Verify on Docker Hub that the image is uploaded as the ``tagname`` you provided (``latest`` if you ommit it).

## CI/CD

### Docker Hub Access Token (highly recommended)

In you Docker Hub account, create an [access token](https://docs.docker.com/docker-hub/access-tokens/). **Remember the value!**

### Gitlab pipeline

__TODO__ Write doc

* In the gitlab repository, under ``Settings > CI/CD``, add 2 new **Variables**:
  * ``DOCKER_HUB_USER`` - Value is your Docker Hub username
  * ``DOCKER_HUB_ACCESS_TOKEN`` - Value is the token you created above
* [gitlab-ci.yml](gitlab-ci.yml)

### Azure App Service

__TODO__ Write doc

* [Enable contiuous deployment](https://learn.microsoft.com/en-us/azure/app-service/deploy-ci-cd-custom-container?tabs=acr&pivots=container-linux#4-enable-cicd) and [configure Docker Hub with provided webhook](https://docs.docker.com/docker-hub/webhooks/).
