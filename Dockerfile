FROM node:19-alpine 

# Create app directory
WORKDIR /usr/src/demo-app

# Install app dependencies
COPY package*.json ./
RUN npm install -g npm@8.0.0 && \
    npm install

# Bundle app source
COPY . .

# Expose default React app port
EXPOSE 3000

# Provide command to run app
CMD [ "npm", "start" ]